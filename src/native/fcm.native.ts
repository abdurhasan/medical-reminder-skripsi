import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {FCMPlugin} from 'cordova-plugin-fcm-with-dependecy-updated';

declare const FCM: FCMPlugin;

export interface IChannelConfiguration {
  id: string
  name: string
  description?: string
  importance?: 'none' | 'min' | 'low' | 'default' | 'high'
  visibility?: 'public' | 'private' | 'secret'
  sound?: string
  lights?: boolean
  vibration?: boolean
}

export interface INotificationPayload {
  wasTapped: boolean

  [others: string]: any
}

export interface IRequestPushPermissionOptions {
  ios9Support?: {
    timeout?: number
    interval?: number
  }
}

@Injectable()
export class FcmNative {

  public clearAllNotifications(): Promise<void> {
    return FCM.clearAllNotifications()
  }

  public createNotificationChannel(channelConfig: IChannelConfiguration): Promise<void> {
    return FCM.createNotificationChannel(channelConfig)
  }

  public deleteInstanceId(): Promise<void> {
    return FCM.deleteInstanceId()
  }

  public getAPNSToken(): Promise<string> {
    return FCM.getAPNSToken()
  }

  /** @copyFrom typings/FCMPlugin.d.ts FCMPlugin getInitialPushPayload */
  public getInitialPushPayload(): Promise<INotificationPayload | null> {
    return FCM.getInitialPushPayload()
  }

  /** @copyFrom typings/FCMPlugin.d.ts FCMPlugin getToken */
  public getToken(): Promise<string> {
    try {
      return FCM.getToken()
    } catch (error) {
      return Promise.reject(JSON.stringify(error));
    }
  }

  public async getAsyncToken(): Promise<string> {
    try {
      return await FCM.getToken()
    } catch (error) {
      return await Promise.reject(JSON.stringify(error));
    }
  }

  /** @copyFrom typings/FCMPlugin.d.ts FCMPlugin hasPermission */
  public hasPermission(): Promise<boolean> {
    return FCM.hasPermission()
  }

  /** @copyFrom ionic/FCM.d.ts FCMPluginOnIonic onNotification */
  public onNotification(options?: { once?: boolean }): Observable<INotificationPayload> {
    const observable = new Subject<INotificationPayload>()
    const handler = (payload: INotificationPayload) => observable.next(payload)
    FCM.onNotification(handler, options)

    return observable
  }

  /** @copyFrom ionic/FCM.d.ts FCMPluginOnIonic onTokenRefresh */
  public onTokenRefresh(options?: { once?: boolean }): Observable<string> {
    const observable = new Subject<string>()
    FCM.onTokenRefresh((token: string) => observable.next(token), options)

    return observable
  }

  /** @copyFrom typings/FCMPlugin.d.ts FCMPlugin requestPushPermission */
  public requestPushPermission(options?: IRequestPushPermissionOptions): Promise<boolean> {
    return FCM.requestPushPermission(options)
  }

  /** @copyFrom typings/FCMPlugin.d.ts FCMPlugin subscribeToTopic */
  public subscribeToTopic(topic: string): Promise<void> {
    return FCM.subscribeToTopic(topic)
  }

  /** @copyFrom typings/FCMPlugin.d.ts FCMPlugin unsubscribeFromTopic */
  public unsubscribeFromTopic(topic: string): Promise<void> {
    return FCM.unsubscribeFromTopic(topic)
  }
}
