import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LogInfo } from '../../global';
import { AppService } from '../../app/app.service';
import * as isEmpty from 'is-empty';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {

  news: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public appService: AppService,
    public inAppBrowser: InAppBrowser,
  ) {
    this.getNews()

  }

  OpenUrl(link: string) {
    const browser = this.inAppBrowser.create(link)
    browser.show()
  }

  async getNews() {
    const dataNews = await this.appService.runRequest('getNews', {})
    if (!isEmpty(dataNews)) {
      this.news = dataNews
    }
    console.log('this.news : ', this.news)
  }

  ionViewDidLoad() {
    LogInfo('ionViewDidLoad NewsPage');
  }

}
