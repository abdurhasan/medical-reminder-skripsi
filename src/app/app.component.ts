import {Component} from '@angular/core';
import {Events, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {FcmNative} from '../native/fcm.native';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = 'LoginPage';
  notifIds: number[];


  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    public events: Events,
    private splashScreen: SplashScreen,
    private fcm: FcmNative,
  ) {
    this.notifIds = [];
    platform.ready().then(() => {
      statusBar.styleDefault();
    });

    // this.menuController.enable(true)

    // setInterval(() => {
    //   this.events.publish(EventEnums.FORCE_RUN_DAY, null)
    // }, ONE_DAY_MS)

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      if (this.platform.is('cordova')) {
        await this.fcm.subscribeToTopic('people');

        // this.fcm.getToken().then(token => {
        //   console.log('DOR : ', token);
        // });

        this.fcm.onNotification().subscribe(data => {
          console.log(data);
          if (data.wasTapped) {
            console.log('Received in background');
            // this.router.navigate([data.landing_page, data.price]);
          } else {
            console.log('Received in foreground');
            // this.router.navigate([data.landing_page, data.price]);
          }
        });

        this.fcm.onTokenRefresh().subscribe(token => {
          console.log(token);
        });
      }
      // this.fcm.unsubscribeFromTopic('marketing');
    });
  }
}

